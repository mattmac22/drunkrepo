package com.example.drunkcompass;

public interface Observer 
{
	public void notifyOfEvent(int eventCode, String message);

}
