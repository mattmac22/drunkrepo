package com.example.drunkcompass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class CompassView extends ImageView {

	private float direction;
	private float currentDegree = 0f;
	private float bearing;
	private float distance;
	private Location currentLocation;


	public CompassView(Context context) {
		super(context);
		this.init();
	}

	public CompassView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}

	public CompassView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.init();
	}
	private void init()
	{
	}
	public Location getCurrentLocation()
	{
		return this.currentLocation;
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(
				MeasureSpec.getSize(widthMeasureSpec),
				MeasureSpec.getSize(heightMeasureSpec));
	}
    public void rotatePin(float azimuth)
    {
    	//Log.i("TAG","Final degree: " + degreeFinal);
    //	Log.i("TAG","Bearking: " + this.bearing);
    	float degreeFinal = azimuth - this.bearing;
    	degreeFinal = -degreeFinal;
    	RotateAnimation animation = new RotateAnimation(
    			this.currentDegree,
    			degreeFinal,
    			Animation.RELATIVE_TO_SELF, 0.5f,
    			Animation.RELATIVE_TO_SELF,
    			0.5f);
    	animation.setDuration(210);
    	animation.setFillAfter(true);
    	this.startAnimation(animation);
    	this.currentDegree = degreeFinal;
    }
    public void pointToLocation(Location current, Location target)
    {
    	Log.i("TAG","Calculated bearing");
        this.bearing = current.bearingTo(target);
        this.distance = current.distanceTo(target);
        this.currentLocation = current;
        Log.i("TAG","Bearing: " + this.bearing);
        this.rotatePin(this.currentDegree);
    }
    public void setTargetLocation(Location location)
    {
    	
    }


	public void update(float dir){
		this.rotatePin(dir);
//		direction = dir;
		invalidate();
	}

}