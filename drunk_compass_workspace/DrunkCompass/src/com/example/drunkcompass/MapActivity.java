package com.example.drunkcompass;

import java.io.IOException;
import java.util.List;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MapActivity extends FragmentActivity
{
	private GoogleMap map;
	private int ZOOM_INITIAL = 13;
	private int ZOOM_SEARCH = 7;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.map_layout);
		
		Bundle extras = this.getIntent().getExtras();
		boolean showIntro = extras.getBoolean("showIntro");
		if (showIntro)
		{
			this.showIntro();
		}
		
		SupportMapFragment fragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		this.map = fragment.getMap();
		this.centerAtLocation(this.map, MapActivity.getLocation(this), ZOOM_INITIAL);
		this.setMarkerBehavior();
    }
	public void showIntro()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
		builder.setMessage("Press and hold on map to select your home location");
		builder.setPositiveButton("OK got it", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
			}
		});
		builder.create().show();
	}
	public void setMarkerBehavior()
	{
		this.map.setOnMapLongClickListener(new OnMapLongClickListener()
		{
			@Override
			public void onMapLongClick(final LatLng location) 
			{
				// TODO Auto-generated method stub
				final Marker marker = map.addMarker(new MarkerOptions().position(location).draggable(false));
				AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
				builder.setMessage("Would you like to set this as your home location?");
				builder.setNegativeButton("No", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						marker.remove();
					}
				});
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						Intent data = new Intent();
						data.putExtra("latitude", location.latitude);
						data.putExtra("longitude", location.longitude);
						MapActivity.this.setResult(RESULT_OK, data);
						MapActivity.this.finish();
					}
				});
				builder.create().show();
			}
		});
	}
	public void centerAtLocation(GoogleMap map, Location location, float zoom)
	{
		LatLng locationF = new LatLng(location.getLatitude(), location.getLongitude());
		CameraUpdate center = CameraUpdateFactory.newLatLngZoom(locationF, zoom);
		map.moveCamera(center);
	}
	public void searchAddress(final View view)
	{
		EditText text = (EditText) this.findViewById(R.id.searchText);
		String addressString = text.getText().toString();
		Location loc = this.getLocationFromAddress(addressString, this);
		if (loc == null)
		{
			Toast toast= Toast.makeText(getApplicationContext(), 
					"Address Not Found", Toast.LENGTH_SHORT);  
					toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
					toast.show();
			return;
		}
		this.centerAtLocation(this.map, loc, ZOOM_SEARCH);
	}
	public static Location getLocationFromAddress(String address, Activity activity)
	{
		Geocoder geo = new Geocoder(activity);
		List<Address> addressList = null;
		Location loc = null;
		try
		{
			addressList = geo.getFromLocationName(address, 5);
			if (addressList == null || addressList.size() == 0)
			{
				return null;
			}
			Address location = addressList.get(0);
			loc = new Location("dummy_provider");
			loc.setLatitude(location.getLatitude());
			loc.setLongitude(location.getLongitude());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return loc;
	}
	public static Location getLocation(Activity activity)
	{
		Location location = null;
		try
		{
			LocationManager locationManager = (LocationManager)activity.getSystemService(Context.LOCATION_SERVICE); 
			if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			{
				location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}
			else
			{
				location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return location;
	}
}
