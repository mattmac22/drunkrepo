package com.example.drunkcompass;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
public class MyLocationManager 
{
	public static final String PREFS_HOME_LATITUDE = "home_latitude_prefs";
	public static final String PREFS_HOME_LONGITUDE = "home_longitude_prefs";
	public static final String PREFS_TITLE = "prefs";
	
	public static final String[] items = {"Home","McDonalds","Burger King","Pizza","Beer"};
	private static final String[] itemQueries = {"home", "McDonalds","Burger King","Pizza","Beer"};
	private int targetIndex;
	
	private MainActivity activity;
	
	private Location targetLocation;
	
	public MyLocationManager(MainActivity activity)
	{
		Log.i("TAG","CREATING MANAGER");
		this.activity = activity;
		this.targetIndex = 0;
		this.initializeTargetLocation();
	}
	public Location getTargetLocation()
	{
		return this.targetLocation;
	}
	public int getTargetIndex()
	{
		return targetIndex;
	}
	public String getSelectedItem()
	{
		return items[this.targetIndex];
	}
    public void initializeTargetLocation()
    {
    	Location home = this.retrieveHomeLocation();
    	Log.i("TAG","RETRIEVIGN HOME LOCAITON");
    	this.targetLocation = home;
    	if (home == null)
    	{
    		Log.i("TAG","it is null");
    		// Set a dummy location
            this.targetLocation = new Location(android.location.LocationManager.GPS_PROVIDER);
            targetLocation.setLatitude(39.9493);
            targetLocation.setLongitude(-75.1667);
    		
    		Intent intent = new Intent(activity, MapActivity.class);
    		intent.putExtra("showIntro", true);
    		activity.startActivityForResult(intent, MainActivity.RESULT_HOME_LOCATION);
    	}
    	else
    	{
    		Log.i("TAG","LOCATION: " + home.getLatitude() + " - " + home.getLongitude());
    	}
    }
	public void setTargetLocationByIndex(final int index, final Observer observer)
	{
		final String query = itemQueries[index];
		final Location homeLocation = this.retrieveHomeLocation();
		if (query.equals("home"))
		{
			this.targetLocation = homeLocation;
			observer.notifyOfEvent(MainActivity.EVENT_SET_TARGET_LOCATION, "Location set to home");
		}
		final Location currentLocation = this.activity.getCurrentLocation();
		new AsyncTask<String, String, String>() {
			@Override
			protected String doInBackground(String... params) 
			{
				Yelp yelp = new Yelp();
				String result = yelp.search(query, currentLocation.getLatitude(), currentLocation.getLongitude());
				return result;
			}
			@Override
			protected void onPostExecute(String result)
			{
				Location location = this.extractLocationFromJSON(result);
				if (location != null)
				{
					MyLocationManager.this.targetLocation = location;
					MyLocationManager.this.targetIndex = index;
				}
				else
				{
					MyLocationManager.this.alertFailedSearch(query);
				}
				Log.i("TAG","Alerting of location");
				observer.notifyOfEvent(MainActivity.EVENT_SET_TARGET_LOCATION, "Target location set");
			}
			public Location extractLocationFromJSON(String JSONString)
			{
				Log.i("TAG",JSONString);
				Location geoLoc = null;
				JSONObject obj;
				try {
					obj = new JSONObject(JSONString);
					JSONArray businesses = obj.getJSONArray("businesses");
					if (businesses.length() == 0)
					{
						Log.i("TAG","BUSINESSES ARE NULL");
						return null;
					}
					JSONObject business = businesses.getJSONObject(0);

					String name = business.getString("name");
					JSONObject location = business.getJSONObject("location");
					JSONArray addressArray = location.getJSONArray("address");
					
					String address = this.addressArrayToString(addressArray);
					if (address == null) // Go by the name
					{
						address = business.getString("name");
					}
					
					String city = location.getString("city");
					String state = location.getString("state_code");
					String fullAddress = address + ", " + city + ", " + state;
					Log.i("TAG",fullAddress);
					geoLoc = MapActivity.getLocationFromAddress(fullAddress, MyLocationManager.this.activity);
					if (geoLoc == null)
					{
						Log.i("TAG","GEOLOC WAS NULL");
						return null;
					}
					Log.i("TAG", "LAT: " + geoLoc.getLatitude());
					Log.i("TAG", "LONG: " + geoLoc.getLongitude());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return geoLoc;
			}
			public String addressArrayToString(JSONArray arr)
			{
				Log.i("TAG","ARRAY LENGTH: " + arr.length());
				try {
					if (arr.length() == 1)
					{
						return removeCommaPart(arr.getString(0));
					}
					else
					{
						return removeCommaPart(arr.getString(arr.length() - 1));
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
			private String removeCommaPart(String address)
			{
				String[] parts = address.split(",");
				return parts[0];
			}
		}.execute(null,null,null);
	}
	public void alertFailedSearch(String query)
	{
    	AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
    	builder.setTitle("We could not find " + query);
    	builder.setPositiveButton("Ok", null);
    	builder.create().show();
	}
	public void setHomeLocation(Location location)
	{
		if (this.items[this.targetIndex].equals("Home"))
		{
			this.targetLocation = location;
		}
		this.storeHomeLocation(location);
	}
    public void storeHomeLocation(Location location)
    {
    	SharedPreferences prefs = activity.getSharedPreferences(MyLocationManager.PREFS_TITLE, 0);
    	prefs.edit().putLong(MyLocationManager.PREFS_HOME_LATITUDE, Double.doubleToLongBits(location.getLatitude())).commit();
    	prefs.edit().putLong(MyLocationManager.PREFS_HOME_LONGITUDE, Double.doubleToLongBits(location.getLongitude())).commit();
    }
    public Location retrieveHomeLocation()
    {
    	SharedPreferences prefs = activity.getSharedPreferences(MyLocationManager.PREFS_TITLE, 0);
    	long latitudeLong = prefs.getLong(MyLocationManager.PREFS_HOME_LATITUDE, -1);
    	long longitudeLong = prefs.getLong(MyLocationManager.PREFS_HOME_LONGITUDE, -1);
    	if (latitudeLong == -1 && longitudeLong == -1)
    	{
    		return null;
    	}
    	double latitude = Double.longBitsToDouble(latitudeLong);
    	double longitude = Double.longBitsToDouble(longitudeLong);
    	Location location = new Location("dummy_provider");
    	location.setLatitude(latitude);
    	location.setLongitude(longitude);
    	return location;
    }
}
