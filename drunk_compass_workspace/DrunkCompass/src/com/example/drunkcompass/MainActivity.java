package com.example.drunkcompass;

import java.text.DecimalFormat;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener, Observer
{
	private ImageView pinImage;

	private SensorManager sensorManager;
	private Sensor sensorAccelerometer;
	private Sensor sensorMagneticField;

	private float[] valuesAccelerometer;
	private float[] valuesMagneticField;

	private float[] matrixR;
	private float[] matrixI;
	private float[] matrixValues;

	private Location currentLocation;
	private TextView distanceText;

	public static final int RESULT_HOME_LOCATION = 1;
	
	public static final int EVENT_SET_TARGET_LOCATION = 2;
	
	private MyLocationManager locManager;


	CompassView compass;
	 
	class MyLocationListener implements LocationListener
	{
		@Override
	    public void onLocationChanged(Location current) 
		{
	        currentLocation = current;
	        MainActivity.this.refreshDisplay();
	        Log.i("TAG","Lat: " + currentLocation.getLatitude());
	        Log.i("TAG","Long: " + currentLocation.getLongitude());
	    }

	    @Override
	    public void onProviderDisabled(String provider) {}

	    @Override
	    public void onProviderEnabled(String provider) {}

	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras) {}
	}

	public void updateDistance()
	{
		float distance = this.currentLocation.distanceTo(this.locManager.getTargetLocation());
		DecimalFormat df = new DecimalFormat("#.00"); 
		String message = "Distance: " + df.format(this.metersToMiles(distance)) + " miles";
		this.distanceText.setText(message);
	}
	public double metersToMiles(float meters)
	{
		return meters/1609.34;
	}
	public void setDestinationName(String name)
	{
		TextView text = (TextView) this.findViewById(R.id.destinationText);
		String title = "Destination: " + name;
		text.setText(title);
	}
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compass_layout);
        
        this.valuesAccelerometer = new float[3];
        this.valuesMagneticField = new float[3];
       
        this.matrixR = new float[9];
        this.matrixI = new float[9];
        this.matrixValues = new float[3];
        
        
        
        this.sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        this.sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        this.distanceText = (TextView) this.findViewById(R.id.distanceTextView);
    
        this.compass = (CompassView) this.findViewById(R.id.compass);
        
        LocationListener locationListener = new MyLocationListener();
        LocationManager locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        
        this.locManager = new MyLocationManager(this);

        currentLocation = MapActivity.getLocation(this);
        this.refreshDisplay();
        
        this.locManager.initializeTargetLocation();
    }
    public Location getCurrentLocation()
    {
    	return this.currentLocation;
    }
    public void refreshDisplay()
    {
    	compass.pointToLocation(this.currentLocation, this.locManager.getTargetLocation());
    	MainActivity.this.updateDistance();
    }
    public void setHomeLocation(final View view)
    {
    	Intent intent = new Intent(this, MapActivity.class);
		intent.putExtra("showIntro", false);
    	this.startActivityForResult(intent, RESULT_HOME_LOCATION);
    }

    public void changeDestination(final View view)
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("Select the Destination");
    	builder.setSingleChoiceItems(MyLocationManager.items, this.locManager.getTargetIndex(), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				MainActivity.this.locManager.setTargetLocationByIndex(which, MainActivity.this);
				dialog.cancel();
			}
		});
    	builder.create().show();
    }
    @Override
    protected void onResume() 
    {
    	sensorManager.registerListener(this,
    			sensorAccelerometer,
    			SensorManager.SENSOR_DELAY_NORMAL);
    	sensorManager.registerListener(this,
    			sensorMagneticField,
    			SensorManager.SENSOR_DELAY_NORMAL);
    	super.onResume();
    }
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.i("TAG","Result deliverde");
		switch (requestCode)
		{
		case MainActivity.RESULT_HOME_LOCATION:
			// Add the new widget if a new plan was added
			if (resultCode == RESULT_OK)
			{
				double latitude = data.getDoubleExtra("latitude", -1);
				double longitude = data.getDoubleExtra("longitude", -1);
				Log.i("TAG","location: " + latitude + " - " + longitude);
				Location location = new Location("dummy_provider");
				location.setLatitude(latitude);
				location.setLongitude(longitude);
				this.locManager.setHomeLocation(location);
				this.refreshDisplay();
			}
			break;
		}
	}
    
    @Override
    protected void onPause() 
    {   
    	sensorManager.unregisterListener(this,
    			sensorAccelerometer);
    	sensorManager.unregisterListener(this,
    			sensorMagneticField);
    	super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
	@Override
	public void notifyOfEvent(int eventCode, String message) 
	{
		Log.i("TAG",message);
		switch (eventCode)
		{
		case MainActivity.EVENT_SET_TARGET_LOCATION:
			Log.i("TAG","About to set");
			this.setDestinationName(this.locManager.getSelectedItem());
			this.refreshDisplay();
		}
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
    @Override
    public void onSensorChanged(SensorEvent event) {
    	if (this.compass.getCurrentLocation() == null)
    	{
    		return;
    	}
    	// TODO Auto-generated method stub
    	switch(event.sensor.getType())
    	{
    	case Sensor.TYPE_ACCELEROMETER:
    		for(int i =0; i < 3; i++){
    			valuesAccelerometer[i] = event.values[i];
    		}
    		break;
    	case Sensor.TYPE_MAGNETIC_FIELD:
    		for(int i =0; i < 3; i++){
    			valuesMagneticField[i] = event.values[i];
    		}
    		break;
    	}

    	boolean success = SensorManager.getRotationMatrix(
    			matrixR,
    			matrixI,
    			valuesAccelerometer,
    			valuesMagneticField);

    	if(success)
    	{
    		SensorManager.getOrientation(matrixR, matrixValues);

    		double azimuth = Math.toDegrees(matrixValues[0]);
    		double pitch = Math.toDegrees(matrixValues[1]);
    		double roll = Math.toDegrees(matrixValues[2]);

    		//this.rotatePin(matrixValues[0]);
    		GeomagneticField geoField = new GeomagneticField(
    				(float) this.compass.getCurrentLocation().getLatitude(),
    				(float) this.compass.getCurrentLocation().getLongitude(),
    				(float) this.compass.getCurrentLocation().getAltitude(),
    				System.currentTimeMillis());
    		azimuth += geoField.getDeclination();
    		this.compass.update((float)azimuth);
    	}

    }

    
}
