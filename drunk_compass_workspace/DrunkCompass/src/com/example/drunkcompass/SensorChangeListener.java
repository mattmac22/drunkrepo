package com.example.drunkcompass;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class SensorChangeListener implements SensorEventListener
{
	private float[] valuesAccelerometer;
	private float[] valuesMagneticField;

	private float[] matrixR;
	private float[] matrixI;
	private float[] matrixValues;
	
	CompassView compass;
	public SensorChangeListener(CompassView compass)
	{
		this.compass = compass;
		
        this.valuesAccelerometer = new float[3];
        this.valuesMagneticField = new float[3];
       
        this.matrixR = new float[9];
        this.matrixI = new float[9];
        this.matrixValues = new float[3];
	}
	
    @Override
    public void onSensorChanged(SensorEvent event) {
    	Log.i("TAG","Sensor changed");
    	if (this.compass.getCurrentLocation() == null)
    	{
    		return;
    	}
    	// TODO Auto-generated method stub
    	switch(event.sensor.getType())
    	{
    	case Sensor.TYPE_ACCELEROMETER:
    		for(int i =0; i < 3; i++){
    			valuesAccelerometer[i] = event.values[i];
    		}
    		break;
    	case Sensor.TYPE_MAGNETIC_FIELD:
    		for(int i =0; i < 3; i++){
    			valuesMagneticField[i] = event.values[i];
    		}
    		break;
    	}

    	boolean success = SensorManager.getRotationMatrix(
    			matrixR,
    			matrixI,
    			valuesAccelerometer,
    			valuesMagneticField);

    	if(success)
    	{
    		SensorManager.getOrientation(matrixR, matrixValues);

    		double azimuth = Math.toDegrees(matrixValues[0]);
    		double pitch = Math.toDegrees(matrixValues[1]);
    		double roll = Math.toDegrees(matrixValues[2]);

    		//this.rotatePin(matrixValues[0]);
    		GeomagneticField geoField = new GeomagneticField(
    				(float) this.compass.getCurrentLocation().getLatitude(),
    				(float) this.compass.getCurrentLocation().getLongitude(),
    				(float) this.compass.getCurrentLocation().getAltitude(),
    				System.currentTimeMillis());
    		azimuth += geoField.getDeclination();
    		this.compass.update((float)azimuth);
    	}

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    	// TODO: What is this
    }
}
